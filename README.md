EECE 210 Example 2: The Christmas Song
========

This program illustrates a few basic Java concepts:
* String concatenation <http://docs.oracle.com/javase/tutorial/java/data/strings.html>
* Arrays <http://docs.oracle.com/javase/tutorial/java/nutsandbolts/arrays.html>
* static methods: these are similar to functions in C but not identical.
* There is also an example of a regular comment and a Javadoc comment. Javadoc comments are used to automatically generate documentation for your code. You should use Javadoc comments effectively.
* Java is extensively documented on the Oracle website (the links above are examples of this documentation). You should learn to use this documentation efficiently.

Clone this repository to your own machine. Then make the changes as suggested in the program. The changes needed are also highlighted using a TODO comment. Compile and run the program first, then make changes and try again.

This program is supposed to print the classic Christmas Song for the first six days:
```
On the 1st day of Christmas, my true love gave to me 
a partridge in a pear tree.

On the 2nd day of Christmas, my true love gave to me 
two turtle doves, and
a partridge in a pear tree.

On the 3rd day of Christmas, my true love gave to me 
three French hens,
two turtle doves, and
a partridge in a pear tree.

On the 4th day of Christmas, my true love gave to me 
four calling birds,
three French hens,
two turtle doves, and
a partridge in a pear tree.

On the 5th day of Christmas, my true love gave to me 
five golden rings,
four calling birds,
three French hens,
two turtle doves, and
a partridge in a pear tree.

On the 6th day of Christmas, my true love gave to me 
six geese a-laying,
five golden rings,
four calling birds,
three French hens,
two turtle doves, and
a partridge in a pear tree.
```